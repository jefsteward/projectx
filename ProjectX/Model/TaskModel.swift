//
//  TaskModel.swift
//  ProjectX
//
//  Created by Jeffry Steward W on 08/03/20.
//  Copyright © 2020 Jeffry Steward W. All rights reserved.
//

import Foundation

class TaskModel: NSObject {
    var task = ""
    var status = ""
    
    init(task: String, status: String) {
        self.task = task
        self.status = status
    }
    
}

struct TaskModelStr {
    var dataArray = [TaskModel]()
}
