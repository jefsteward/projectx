//
//  AddTaskViewController.swift
//  ProjectX
//
//  Created by Jeffry Steward W on 05/03/20.
//  Copyright © 2020 Jeffry Steward W. All rights reserved.
//

import UIKit

class AddTaskViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var addTaskTextView: UITextView!
    @IBOutlet weak var statusSegmentedControl: UISegmentedControl!
    @IBOutlet weak var doneButton: UIButton!
    
    let defaultTaskText: String! = "Say something..."
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        statusSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func done(_ sender: UIButton) {
        
        print("done")
        if let navController = presentingViewController as? UINavigationController {
           let presenter = navController.topViewController as! TaskTableViewController
            presenter.myModel.insert(TaskModel(task: addTaskTextView.text, status:
                statusSegmentedControl.titleForSegment(at: statusSegmentedControl.selectedSegmentIndex)!),at: 0)
            presenter.tableView.reloadData()
        }
        dismiss(animated: true)
    }
    
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if addTaskTextView.text! == "Say something..." {
            addTaskTextView.text.removeAll()
            addTaskTextView.textColor = .white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if addTaskTextView.text == "" {
            addTaskTextView.text = self.defaultTaskText
            addTaskTextView.textColor = .white
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.endEditing(true)
            return false
        }
        else {
            return true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
