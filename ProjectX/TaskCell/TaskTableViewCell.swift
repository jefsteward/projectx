//
//  TaskTableViewCell.swift
//  ProjectX
//
//  Created by Jeffry Steward W on 08/03/20.
//  Copyright © 2020 Jeffry Steward W. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    
    var myModel: TaskModel? {
        didSet {
            cellConfig()
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellConfig() {
        guard let obj = myModel else { return }
        taskLabel.text = obj.task
        statusLabel.text = obj.status
        if statusLabel.text == "Pending" {
            statusImageView.image = #imageLiteral(resourceName: "pending")
        }
        else if statusLabel.text == "In Progress" {
            statusImageView.image = #imageLiteral(resourceName: "in_progress")
        }
        else if statusLabel.text == "Completed" {
            statusImageView.image = #imageLiteral(resourceName: "completed")
        }
        
    }
    
}
