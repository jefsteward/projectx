//
//  TaskTableViewController.swift
//  ProjectX
//
//  Created by Jeffry Steward W on 08/03/20.
//  Copyright © 2020 Jeffry Steward W. All rights reserved.
//

import UIKit

class TaskTableViewController: UITableViewController {
    
    @IBOutlet var taskTabelView: UITableView!
    var myModel = [TaskModel]()
    private var dataCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupModel()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func setupUI() {
        taskTabelView.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskChildCell")
    }
    
    func setupModel() {
        
        myModel.append(TaskModel(task: "Learn Color Harmony", status: "Pending"))
        myModel.append(TaskModel(task: "Register Apple Developer Account", status: "Pending"))
        myModel.append(TaskModel(task: "Nano Challenge", status: "In Progress"))
        myModel.append(TaskModel(task: "Reflection", status: "Completed"))
        myModel.append(TaskModel(task: "Mini Challenge", status: "In Progress"))
        myModel.append(TaskModel(task: "Register Apple Developer Account", status: "Pending"))
        myModel.append(TaskModel(task: "Introduction to Studio", status: "Completed"))
        myModel.append(TaskModel(task: "Summary", status: "Completed"))
        myModel.append(TaskModel(task: "Reflection", status: "Completed"))
        myModel.append(TaskModel(task: "Learn Color Harmony", status: "Pending"))
        myModel.append(TaskModel(task: "Nano Challenge", status: "In Progress"))
        myModel.append(TaskModel(task: "Mini Challenge", status: "In Progress"))
        myModel.append(TaskModel(task: "Introduction to Studio", status: "Completed"))
        myModel.append(TaskModel(task: "Summary", status: "Completed"))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "TaskChildCell", for: indexPath) as? TaskTableViewCell)!
        cell.myModel = myModel[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // TODO: handle delete (by removing the data from your array and updating the tableview)
            myModel.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
//
//    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
//            completion(true)
//        }
//        action.image = .remove
//        action.backgroundColor = .red
//        return UISwipeActionsConfiguration(actions: [action])
//    }
//
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Done") { (action, view, completion) in
            self.myModel.remove(at: indexPath.row)
            completion(true)
            tableView.reloadData()
        }
        action.image = .actions
        action.backgroundColor = .green
        
        return UISwipeActionsConfiguration(actions: [action])
    }
}
